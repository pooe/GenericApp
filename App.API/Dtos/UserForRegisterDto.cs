using System.ComponentModel.DataAnnotations;

namespace App.API.Dtos
{
    public class UserForRegisterDto
    {
       [Required]
        public string Username { get; set; }
        
        [Required]//Validation
        [StringLength(8, MinimumLength = 4, ErrorMessage="You must create a password between 4 and 8 characters")]
        public string Password { get; set; }
    }
}