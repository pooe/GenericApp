using System.Threading.Tasks;
using App.API.Models;

namespace App.API.Data
{
    public interface IAuthRepository
    {
        //Contract methods
        Task<User> Register(User user, string password);
        Task<User> Login(string username, string password);
        Task<bool> UserExists(string username);
    }
}