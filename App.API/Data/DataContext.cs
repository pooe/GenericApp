using App.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Models.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options){}

        public DbSet<Value> Values {get; set; }
        public DbSet<User> Users {get; set; }
    }
}

//Microsoft.EntityframeworkCore
//Microsoft.EntityframeworkCore.Sqlite