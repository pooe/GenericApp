using Microsoft.AspNetCore.Http;

namespace App.API.Helpers
{
    public static class Extensions//Static, Dont need to create new instance to use class
    {
        public static void AddApplicationError(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin", "*");

        }
    }
}